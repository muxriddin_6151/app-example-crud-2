package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.entity.User;
import uz.pdp.repository.ComputerRepository;
import uz.pdp.repository.UserRepository;

import java.util.Optional;

@Controller
public class HomeController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ComputerRepository computerRepository;

    @GetMapping("/")
    public String openLoginPage(){
        return "login";
    }

    @PostMapping("/signin")
    public String isPresent(@RequestParam(defaultValue = "false") boolean wrong, String username, String password,
                            Model model){
        Optional<User> byUsername = userRepository.findByUsername(username);
        if (byUsername.isPresent()){
            model.addAttribute("computers",computerRepository.findAll());
            return "index";
        }else{
            model.addAttribute("wrong",wrong);
            return  "login";
        }
    }




}
