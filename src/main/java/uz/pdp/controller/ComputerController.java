package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.entity.Computer;
import uz.pdp.repository.ComputerRepository;

@Controller
@RequestMapping("/computer")
public class ComputerController {


    @Autowired
    ComputerRepository computerRepository;

    @GetMapping("/add")
    public String addComputer(){
        return "addcomputer";
    }

    @PostMapping("/save")
    public String saveComputer(@RequestParam String name,@RequestParam String model,@RequestParam Integer price,
                               @RequestParam String date, Model aModel){
        Computer computer1=new Computer(name,model,price,date);
        computerRepository.save(computer1);
        aModel.addAttribute("computers", computerRepository.findAll());
        return "index";
    }


}
