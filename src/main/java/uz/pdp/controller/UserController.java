package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.entity.User;
import uz.pdp.repository.ComputerRepository;
import uz.pdp.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ComputerRepository computerRepository;

    @PostMapping("/save")
    public String openLoginPage(@RequestParam String username, @RequestParam String password, Model model){
        userRepository.save(new User(username,password));
        model.addAttribute("computers",computerRepository.findAll());
        return "index";
    }
    @GetMapping("/add")
    public String openAddUserPage(){
        return "addUsers";
    }

}
